�������� �������
��������� ������� "�����"
����: �������� ���������, ������� ������� ��������� � �������������� ������� �������������� "�����".
��������� - ��� �������� � �����, ������, ����������� � ��������, ��������:
https://www.pressa.tv/uploads/posts/2013-09/1379566087_y20-kopiya.jpg

��������� ������� �� Blazor, Pages\Index.razor - main �������� �� ���� �������
����� � ������ Create
--

�������� ������� ������� � ������� ��� ������������ ����������:
�������� ������� �������� ������������ �������, ��������� �����,
�������� ��������, ��������� � �������, ���������� �������, ���������� ���������� � ����.
��� ������������� ����������� ������ �������.

������ � ������ �������� � ������� GeneratorMotivatorLib
--

�������� �������� ����� � ����� �������, ������� ����������
������� ����� ������� ��� ���������� ���������� � ���������� ��� ���������� �����:
string CreateMotivator(string image, string message)

FacadeMotivator.cs
--

����������� ��������� �������� � ��� ��� ������������ ���������.

GeneratorMotivatorLibTests1\images

--

� �������� ��������, ����� ������� �������������� �� ������������.

����� � ������ FacadeMotivator
�������� ����� ���� �� ���������� ������ FacadeMotivator  ���������� ��� ��������� �����, ������������ �������� ������ ����������, ���� ����, ��� ������.
Motivator ����� ���� �� ������� ������������� ���� �� � ���� ��� �� ��� �������� ����� �������� � ������.
Motivator �������� �������� Complete, �������������� ���������� ������ ������ Save. ���� ���� ������������ ��� ���� 
����������� ����������� ��������, ��������� html ��� ����������� �������� ������ ����� ���������� �� �� ������� ����. ��� ����� ������� ��������� ���������.
