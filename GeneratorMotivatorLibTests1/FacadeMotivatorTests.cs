﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace GeneratorMotivatorLib.Tests
{
    [TestClass()]
    public class FacadeMotivatorTests
    {
        private static string path = @"C:\Temp\";

        /// <summary>
        /// Тес однострочного мотиватора
        /// </summary>
        [TestMethod()]
        public void CreateMotivatorTest()
        {
            string pathSource = $"{Directory.GetCurrentDirectory()}{@"\images\godzillа.jpg"}";
            string pathTo = path + @"godzillа.jpg";
            if (!File.Exists(pathTo))
                File.Copy(pathSource, pathTo);
            try
            {
                string savePath = FacadeMotivator.CreateMotivator(pathTo, "Не так страшен черт как его молюют");
                Assert.IsTrue(true, $"Мотиватор сохранен по пути{savePath}");
            }
            catch (Exception e)
            {

                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// Тест двухстрочного мотиватора
        /// </summary>
        [TestMethod()]
        public void CreateEcologyMotivatorTest()
        {
            string pathSource = $"{Directory.GetCurrentDirectory()}{@"\images\Экология.jpg"}";
            string pathTo = path + @"Экология.jpg";
            if (!File.Exists(pathTo))
                File.Copy(pathSource, pathTo);
            try
            {
                string savePath = FacadeMotivator.CreateMotivator(pathTo, "Заботьтесть об экологии", "Или она позаботиться о вас");
                Assert.IsTrue(true, $"Мотиватор сохранен по пути{savePath}");
            }
            catch (Exception e)
            {

                Assert.Fail(e.Message);
            }
        }
    }
}