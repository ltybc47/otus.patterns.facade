﻿using System;
using System.Drawing;
using System.IO;

namespace GeneratorMotivatorLib
{
    /// <summary>
    /// Класс по созданию мотиватора
    /// </summary>
    public class Motivator
    {
        /// <summary>
        /// Импортируемое изображение
        /// </summary>
        public Bitmap BitmapImage { get; set; }
        private Bitmap ImageMotivator { get; set; } = new Bitmap(400, 400);

        public string Name { get; set; } = "default";
        public string FileType { get; set; } = "jpg";

        public string FullName { get 
            {
                previouseName = Name + DateTime.Now.ToString("MMddyyyy_HHmmss") + "." + FileType;
                return previouseName; 
            } 
        }
        private string previouseName;
        public string TextString { get; set; }
        public string TextStringSmall { get; set; }
        public bool Complete { get; set; } = false;

        public void UpdateImage()
        {
            Complete = false;
            Image image = BitmapImage; //путь к картинке
            Image picture = new Bitmap(380, 300);

            Graphics p = Graphics.FromImage(picture);
            p.DrawImage(image, 0, 0);

            Graphics g = Graphics.FromImage(ImageMotivator);
            g.Clear(Color.FromArgb(67, 139, 211));

            g.DrawLines(
                new Pen(Color.White, 4),
                new Point[] {
                new Point(20 - 2, 20 - 2),
                new Point(20 - 2, 300 + 2),
                new Point(380 + 2, 300 + 2),
                new Point(380 + 2, 20 - 2),
                new Point(20 - 4, 20 - 2),
                }
                );
            g.DrawImage(image, 20, 20, 360, 280);

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            if (TextString.Length > 23)
            {
                g.DrawString(TextString, new Font("Verdana", (float)20), new SolidBrush(Color.White), new RectangleF(0, 325, 400, 60), stringFormat);
            }
            else
            {
                g.DrawString(TextString, new Font("Verdana", (float)20), new SolidBrush(Color.White), new RectangleF(0, 325, 400, 30), stringFormat);
                g.DrawString(TextStringSmall, new Font("Verdana", (float)12), new SolidBrush(Color.White), new RectangleF(0, 365, 400, 30), stringFormat);

            }
        }

        public void Save(string path)
        {
            try
            {
                if (File.Exists(path + previouseName))
                    File.Delete(path + previouseName);
                switch (FileType)
                {
                    case "jpg":
                        ImageMotivator.Save(path + FullName, System.Drawing.Imaging.ImageFormat.Jpeg); //путь и имя сохранения файла
                        break;
                    case "png":
                        ImageMotivator.Save(path + FullName, System.Drawing.Imaging.ImageFormat.Png); //путь и имя сохранения файла
                        break;

                }
                Complete = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
