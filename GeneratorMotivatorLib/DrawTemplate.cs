﻿using System.Drawing;

namespace GeneratorMotivatorLib
{
    static class DrawTemplate
    {
        /// <summary>
        /// Метод рисования рамки
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static Graphics MakeBorder(this Graphics g)
        {
            g.DrawLines(
                new Pen(Color.White, 4),
                new Point[] {
                new Point(20 - 2, 20 - 2),
                new Point(20 - 2, 300 + 2),
                new Point(380 + 2, 300 + 2),
                new Point(380 + 2, 20 - 2),
                new Point(20 - 4, 20 - 2),
                }
                );
            return g;
        }
        /// <summary>
        /// Метод добавления надписи на изображение
        /// </summary>
        /// <param name="g"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Graphics AddTextDescription(this Graphics g, string text)
        {
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            g.DrawString(text, new Font("Verdana", (float)20), new SolidBrush(Color.White), new RectangleF(0, 325, 400, 60), stringFormat);
            return g;
        }
        /// <summary>
        /// Метод добавление подстроки
        /// </summary>
        /// <param name="g"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Graphics AddSubTextDescription(this Graphics g, string text)
        {
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            g.DrawString(text, new Font("Verdana", (float)12), new SolidBrush(Color.White), new RectangleF(0, 365, 400, 30), stringFormat);
            return g;
        }
        /// <summary>
        /// Создание фона заданного размера для мотиватора
        /// </summary>
        /// <returns></returns>
        public static Bitmap CreateImageFromMotivator()
        {
            Bitmap template= new Bitmap(400, 400);
            Graphics g = Graphics.FromImage(template);
            g.Clear(Color.FromArgb(67, 139, 211));

            return template;
        }

        /// <summary>
        /// Добавить  картинку в рамку
        /// </summary>
        /// <param name="g"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Graphics AddImageInBorder(this Graphics g, Image image)
        {
            g.DrawImage(image, 20, 20, 360, 280);
            return g;
        }
    }
}
