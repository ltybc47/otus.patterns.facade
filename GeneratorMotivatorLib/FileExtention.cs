﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace GeneratorMotivatorLib
{
    public static class FileExtention
    {
        /// <summary>
        /// Загрузить картинку из файла
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Image LoadImage(string path)
        {
            MemoryStream ms = new MemoryStream();
            var fs = new FileStream(path, FileMode.Open);
            try
            {
                fs.CopyTo(ms);

                return new Bitmap(ms);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            finally
            {
                fs.Dispose();
                ms.Dispose();
            }
        }
        /// <summary>
        /// Загрузить картинку из файла
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async static Task<Image> LoadImageAsync(string path)
        {
            MemoryStream ms = new MemoryStream();
            var fs = new FileStream(path, FileMode.Open);
            try
            {
                fs.CopyTo(ms);

                return await Task.FromResult(new Bitmap(ms));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            finally
            {
                fs.Dispose();
                ms.Dispose();
            }
        }
        /// <summary>
        /// Сохранить картинку
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string SaveImage(Bitmap image, string path)
        {
            Image saveImage = new Bitmap(image);
            string fullPath = path + DateTime.Now.ToString("_MMddyyyy_HHmmss") + ".jpg";
            try
            {
                saveImage.Save(fullPath, System.Drawing.Imaging.ImageFormat.Jpeg); //путь и имя сохранения файла
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                saveImage.Dispose();
            }
            return fullPath;
        }
        /// <summary>
        /// СОхранить временную картинку
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string SaveTempImage(Bitmap image, string path)
        {
            Image saveImage = new Bitmap(image);
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
                saveImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg); //путь и имя сохранения файла
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                saveImage.Dispose();
            }
            return path;
        }
    }
}
