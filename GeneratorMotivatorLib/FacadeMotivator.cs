﻿using System.Drawing;
using System.Linq;

namespace GeneratorMotivatorLib
{
    public static class FacadeMotivator
    {

        public static Bitmap template = DrawTemplate.CreateImageFromMotivator();

        public static string CreateMotivator(string imagePath, string message, string submessage)
        {
            if (string.IsNullOrEmpty(submessage))
            {
                return CreateMotivator(imagePath, message);
            }
            else
            {
                template = DrawTemplate.CreateImageFromMotivator();
                Graphics g = Graphics.FromImage(template);

                g.MakeBorder();

                Image importImage = FileExtention.LoadImage(imagePath);

                g.AddImageInBorder(importImage);

                g.AddTextDescription(message);
                g.AddSubTextDescription(submessage);

                string pathFolder = string.Join("\\", imagePath.Split('\\').Take(imagePath.Split('\\').Count() - 1)) + '\\';

                return FileExtention.SaveImage(template, pathFolder + message);
            }

        }

        public static string CreateMotivator(string imagePath, string message)
        {
            template = DrawTemplate.CreateImageFromMotivator();
            Graphics g = Graphics.FromImage(template);

            g.MakeBorder();

            Image importImage = FileExtention.LoadImage(imagePath);

            g.AddImageInBorder(importImage);

            g.AddTextDescription(message);

            string pathFolder = string.Join("\\", imagePath.Split('\\').Take(imagePath.Split('\\').Count() - 1)) + '\\';

            return FileExtention.SaveImage(template, pathFolder + message);
        }
    }
}
